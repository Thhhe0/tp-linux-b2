# Réalisé par Quentin Ruemeli et Theo Leconte


## TP 5  - Hébergement d'une solution libre et opensource

## [](#installation-dun-hebergeur-vid%C3%A9o-plex)Installation d'un hebergeur vidéo

### [](#sommaire-)Sommaire :



On va installer l'hebergeur vidéo "plex"

```
 I. Installation Manuel 
     a. Pré-requis
     b. Installation d'un serveur media avec Plex
     c. Mise en place d'un reverse proxy avec Nginx
     
 II. Maintient en condition opérationel 
     a. Monitoring des machines avec NetData
     b. Alerting via discord
     c. Backup
         - Partage NFS
         - Automatisation
     
 III. Installation automatisée (Script)
     a. Script d'automatisation
     b. Comment lancer l'installation automatisée
```

### [](#ia-pr%C3%A9-requis-pour-la-mise-en-place-)I.A Pré-requis pour la mise en place :

2 machines Rocky Linux avec :

```
    - 1 carte NAT
    - DHCP configuré
    - Firewall actif
    - SSH fonctionel 
    - Resolution de nom
```

Update du system

```
dnf update -y
```

Installation du dépot Plex :

Modification du fichier `/etc/yum.repos.d/plex.repo` Ajoutez ces lignes :

```
[Plex]
name=Plex
baseurl=https://downloads.plex.tv/repo/rpm/$basearch/
enabled=1
gpgkey=https://downloads.plex.tv/plex-keys/PlexSign.key
gpgcheck=1
```

Configuration de SE Linux :

```
sudo setsebool httpd_can_network_connect on -P
```

### [](#ib-serveur-media-avec-plex-)I.B Serveur media avec "Plex" :

Installation de Plex Media Server :

```
sudo dnf install plexmediaserver -y
```

Configuration du service pour que Plex se lance et démarre au boot :

```
sudo systemctl enable plexmediaserver
sudo systemctl start plexmediaserver
```

On Vérifie que le service est bien lancé avec :

```
sudo systemctl status plexmediaserver
```

Configuration du firewall :

```
sudo firewall-cmd --zone=public --add-port=32400/tcp --permanent
sudo firewall-cmd --zone=public --add-port=32469/tcp --permanent 
sudo firewall-cmd --zone=public --add-port=8324/tcp --permanent
sudo firewall-cmd --zone=public --add-port=3005/tcp --permanent
sudo firewall-cmd --zone=public --add-port=32412/udp --permanent
sudo firewall-cmd --zone=public --add-port=32413/udp --permanent
sudo firewall-cmd --zone=public --add-port=32414/udp --permanent
sudo firewall-cmd --zone=public --add-port=32410/udp --permanent
sudo firewall-cmd --zone=public --add-port=1900/udp --permanent
sudo firewall-cmd --zone=public --add-port=5353/udp --permanent
sudo firewall-cmd --zone=public --add-port=80/tcp --permanent
```

reboot du firewall :

```
sudo firewall-cmd --reload
```

On Test si Plex fonctionne correctement avec un navigateur web a l'adresse :

```
http://"Your_IP":32400/web
```

Vous devriez avoir ce résulat : [![](https://user-content.gitlab-static.net/a0de0773f4a9d443e15dee7abbd5c7f7c8c1e4d0/68747470733a2f2f692e696d6775722e636f6d2f6650564c624d6f2e706e67)](https://user-content.gitlab-static.net/a0de0773f4a9d443e15dee7abbd5c7f7c8c1e4d0/68747470733a2f2f692e696d6775722e636f6d2f6650564c624d6f2e706e67)

Créez votre médiathèque, vous aurez ensuite ce résultat :


[![](https://i.ibb.co/YjHdsGn/plex.jpg)](https://i.ibb.co/YjHdsGn/plex.jpg)


### [](#ic-reverse-proxy-avec-nginx-)I.C Reverse proxy avec "Nginx" :

Installation de nginx

```
sudo dnf install nginx -y
```

Configuration du service pour que Nginx se lance et démarre au boot :

```
sudo systemctl start nginx
sudo systemctl enable nginx
```

Configuration de Nginx pour plex : Modification du fichier `/etc/nginx/conf.d/plex.conf`

ajoutez ces lignes :

```
upstream plex_backend {
        server 127.0.0.1:32400;
        keepalive 32;
}

server {
        listen 80;
        server_name plex.example.com;

        send_timeout 100m; 

        gzip on;
        gzip_vary on;
        gzip_min_length 1000;
        gzip_proxied any;
        gzip_types text/plain text/css text/xml application/xml text/javascript application/x-javascript image/svg+xml;
        gzip_disable "MSIE [1-6]\.";
       	
        client_max_body_size 100M;

        proxy_set_header Host $host;
        proxy_set_header X-Real-IP $remote_addr;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_set_header X-Forwarded-Proto $scheme;
        proxy_set_header Sec-WebSocket-Extensions $http_sec_websocket_extensions;
        proxy_set_header Sec-WebSocket-Key $http_sec_websocket_key;
        proxy_set_header Sec-WebSocket-Version $http_sec_websocket_version;

        proxy_http_version 1.1;
        proxy_set_header Upgrade $http_upgrade;
        proxy_set_header Connection "Upgrade";

        proxy_redirect off;
        proxy_buffering off;

        location / {
                proxy_pass http://plex_backend;
        }
}
```

modification de ce fichier : `/etc/nginx/nginx.conf`

ajoutez ces lignes :

```
include /etc/nginx/sites-enabled/*;
server_names_hash_bucket_size 64;
```

on vérifie qu'il n'y a aucune erreur :

```
nginx -t
```

on redemarre Nginx et on affiche son status pour vérifier que tout marche correctement :

```
sudo systemctl restart nginx
sudo systemctl status nginx
```

### [](#iia-monitoring-des-machines-avec-netdata-)II.A Monitoring des machines avec NetData :

Installation de NetData : passez en root avec `su -`

```
bash <(curl -Ss https://my-netdata.io/kickstart.sh)
```

quittez root avec `exit`

Configuration du service NetData :

```
sudo systemctl start netdata
sudo systemctl enable netdata
sudo systemctl status netdata
sudo systemctl is-enabled netdata
```

On utilise la commande `ss` :

```
ss -ltn
```

normalement le port affiché est `19999`

On autorise se port dans dans le firewall :

```
sudo firewall-cmd --add-port=19999/tcp
```

Vous pouvez accéder a l'interface NetData depuis votre navigateur a l'adresse :

```
http://Your_IP:19999
```

### [](#iib-alerting-via-discord-)II.B Alerting via Discord :

Ajoutez un Webhook dans le serveur ou voulez recevoir les notifications :

-   Accédez aux paramètres de votre serveur [![](https://user-content.gitlab-static.net/b43aaf75b72243b759e892368a53e8cb57a9e4bf/68747470733a2f2f692e696d6775722e636f6d2f593330553631612e706e67)](https://user-content.gitlab-static.net/b43aaf75b72243b759e892368a53e8cb57a9e4bf/68747470733a2f2f692e696d6775722e636f6d2f593330553631612e706e67)
    
-   Allez dans "intégrations" [![](https://user-content.gitlab-static.net/ef9b131ced0efe387947b4c53161a7f7ae4997e9/68747470733a2f2f692e696d6775722e636f6d2f7738415457676a2e706e67)](https://user-content.gitlab-static.net/ef9b131ced0efe387947b4c53161a7f7ae4997e9/68747470733a2f2f692e696d6775722e636f6d2f7738415457676a2e706e67)
    
-   Crééez votre Webhook [![](https://user-content.gitlab-static.net/3f9fc72017bc5a72e657282c889863fcb20f5836/68747470733a2f2f692e696d6775722e636f6d2f6672486a4a47632e706e67)](https://user-content.gitlab-static.net/3f9fc72017bc5a72e657282c889863fcb20f5836/68747470733a2f2f692e696d6775722e636f6d2f6672486a4a47632e706e67)
    
-   [![](https://i.ibb.co/GtLVKxM/Capture-d-e-cran-2022-12-13-a-19-55-18.png)](https://i.ibb.co/GtLVKxM/Capture-d-e-cran-2022-12-13-a-19-55-18.png)
    

Modification de la configuration de NetData :

```
sudo vim /etc/netdata/health_alarm_notify.conf

# sending discord notifications

  # note: multiple recipients can be given like this:
  #                  "CHANNEL1 CHANNEL2 ..."

  # enable/disable sending discord notifications
  SEND_DISCORD="YES"

  # Create a webhook by following the official documentation -
  # https://support.discordapp.com/hc/en-us/articles/228383668-Intro-to-Webhooks
  DISCORD_WEBHOOK_URL="link to your discord webhook"

  # if a role's recipients are not configured, a notification will be send to
  # this discord channel (empty = do not send a notification for unconfigured
  # roles):
  DEFAULT_RECIPIENT_DISCORD="alarms"
```

On test le system avec cette commande :

```
sudo bash -x /usr/libexec/netdata/plugins.d/alarm-notify.sh test "sysadmin"
```

Nous allons faire en sorte de recevoir une alerte en fonction de la RAM utilisée, a vous de paramétrer ce que vous voulez

modification du fichier `/etc/netdata/health.d/ram-usage.conf`

ajoutez ces lignes :

```
alarm: ram_usage
    on: system.ram
lookup: average -1m percentage of used
 units: %
 every: 1m
  warn: $this > 50
  crit: $this > 90
  info: The percentage of RAM being used by the system.
```

On test en effectuant un "Stress" sur la machine :

```
stress --vm 2 --timeout 120#Résultat
```

### [](#iic-backup-)II.C Backup :

#### [](#partage-nfs)Partage NFS

**Setup de l'environnement (sur la machine de backup) :**

on installe NFS :

```
dnf -y install nfs-utils
```

Création du fichier `/srv/backup/"Machine_Client"`

Modification du fichier `/etc/idmapd.conf` ajoutez ces lignes :

```
Domain = "Machine_backup"
```

Modification du fichier `/etc/exports` ajoutez ces lignes :

```
# create new
# for example, set [/home/nfsshare] as NFS share
/svr/backup/"Votre_Machine" "ip_du_réseau_hôte"(rw,no_root_squash)
```

on active le service au boot :

```
systemctl enable --now rpcbind nfs-server
```

on modifie le firewall pour le NFS :

```
firewall-cmd --add-service=nfs
```

**Setup de l'environnement (sur la machine du client) :**

on installe NFS :

```
dnf -y install nfs-utils
```

Creation du fichier `/srv/backup/`

Modification du fichier `/etc/idmapd.conf` Ajoutez ces lignes :

```
Domain = "votre VM backup"

sudo mount -t nfs "VM Backup":/srv/backup/"VM Client" /srv/backup/

#Vérifier s'il reste de la place
df -h

#Monter la partition automatiquement
sudo vim /etc/fstab
[...]
"VM Backup":/srv/backup/"VM Client" /srv/backup/              nfs     defaults        0 0
```

#### [](#automatisation)Automatisation

Creation d'un script de backup :

```
#!/bin/bash
# backup script for linux
# "Your name + Date"

dest=$1
tarName="$(date '+"nom_%Y%m%d_%T' | tr -d :).tar.gz"

tar cvfz $tarName $2 #&>/dev/null

rsync -av --remove-source-files "$tarName" $dest #&>/dev/null
```

Creation d'un service pour la backup :

```
sudo touch plex_backup.service
```

Configuration du fichier backup.service :

```
[Unit]
Description=Our own lil backup service (TP2)

[Service]
ExecStart=/srv/plex_backup.sh /mnt/backup /home/"user"/testfolder
Type=oneshot
RemainAfterExit=no

[Install]
WantedBy=multi-user.target
```

On demarre le service :

```
sudo systemctl daemon-reload
sudo systemctl start plex_backup
```

Creation d'un Timer pour le service :

Creation du fichier `plex_backup.timer` Ajoutez ces lignes :

```
[Unit]
Description=Periodically run our plex backup script
Requires=plex_backup.service

[Timer]
Unit=plex_backup.service
OnCalendar=*-*-* *:*:00

[Install]
WantedBy=timers.target
```

On relance le service, on l'active au démarrage et on affiche son status pour vérifier :

```
sudo systemctl daemon-reload
sudo systemctl start plex_backup.timer
sudo systemctl status plex_backup.timer
```

### [](#iii-installation-automatis%C3%A9e-)III. Installation automatisée :

#### [](#script-pour-linstallation-du-serveur-)Script pour l'installation du serveur :

creation du fichier `Install_server.sh`

```
#!/bin/bash
# Install server
# "THEO & QUENTIN 11/12/2022"

function plex_repo(){
    touch /etc/yum.repos.d/plex.repo
    "[Plex]" >> /etc/yum.repos.d/plex.repo
    "name=Plex" >> /etc/yum.repos.d/plex.repo
    "baseurl=https://downloads.plex.tv/repo/rpm/$basearch/" >> /etc/yum.repos.d/plex.repo
    "enabled=1" >> /etc/yum.repos.d/plex.repo
    "gpgkey=https://downloads.plex.tv/plex-keys/PlexSign.key" >> /etc/yum.repos.d/plex.repo
    "gpgcheck=1" >> /etc/yum.repos.d/plex.repo
}

plex_repo


#installation et démarrage de plex
dnf install plexmediaserver -y
systemctl enable plexmediaserver
systemctl start plexmediaserver

#Vérification 
systemctl status plexmediaserver

#Configuration du firewall
firewall-cmd --zone=public --add-port=32400/tcp --permanent
firewall-cmd --zone=public --add-port=32469/tcp --permanent 
firewall-cmd --zone=public --add-port=8324/tcp --permanent
firewall-cmd --zone=public --add-port=3005/tcp --permanent
firewall-cmd --zone=public --add-port=32412/udp --permanent
firewall-cmd --zone=public --add-port=32413/udp --permanent
firewall-cmd --zone=public --add-port=32414/udp --permanent
firewall-cmd --zone=public --add-port=32410/udp --permanent
firewall-cmd --zone=public --add-port=1900/udp --permanent
firewall-cmd --zone=public --add-port=5353/udp --permanent
firewall-cmd --zone=public --add-port=80/tcp --permanent

#reboot firewall
firewall-cmd --reload

# Configuration de SE Linux
setsebool httpd_can_network_connect on -P

read -p 'Ip: ' ipVar
xdg-open http://ipVar:32400/web
```

#### [](#script-pour-linstallation-du-reverse-proxy-)Script pour l'installation du reverse proxy :

```
ReverseProxy.sh

#!/bin/bash
# Install reverse proxy
# "THEO & QUENTIN 11/12/2022"

#Installation de nginx
dnf install nginx -y

#Strat nginx
systemctl start nginx
systemctl enable nginx

function plex_conf(){
    touch /etc/nginx/conf.d/plex.conf
    "upstream plex_backend {" >> /etc/nginx/conf.d/plex.conf
        "server 127.0.0.1:32400;" >> /etc/nginx/conf.d/plex.conf
        "keepalive 32;" >> /etc/nginx/conf.d/plex.conf
    "}" >> /etc/nginx/conf.d/plex.conf

"server {" >> /etc/nginx/conf.d/plex.conf
        "listen 80;" >> /etc/nginx/conf.d/plex.conf
        "server_name plex.example.com;" >> /etc/nginx/conf.d/plex.conf

        "send_timeout 100m;" >> /etc/nginx/conf.d/plex.conf 

        "gzip on;" >> /etc/nginx/conf.d/plex.conf
        "gzip_vary on;" >> /etc/nginx/conf.d/plex.conf
        "gzip_min_length 1000;" >> /etc/nginx/conf.d/plex.conf
        "gzip_proxied any;" >> /etc/nginx/conf.d/plex.conf
        "gzip_types text/plain text/css text/xml application/xml text/javascript application/x-javascript image/svg+xml;" >> /etc/nginx/conf.d/plex.conf
        "tgzip_disable "MSIE [1-6]\.";" >> /etc/nginx/conf.d/plex.conf

        "client_max_body_size 100M;" >> /etc/nginx/conf.d/plex.conf

        "proxy_set_header Host $host;" >> /etc/nginx/conf.d/plex.conf
        "proxy_set_header X-Real-IP $remote_addr;" >> /etc/nginx/conf.d/plex.conf
        "proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;" >> /etc/nginx/conf.d/plex.conf
        "proxy_set_header X-Forwarded-Proto $scheme;" >> /etc/nginx/conf.d/plex.conf
        "proxy_set_header Sec-WebSocket-Extensions $http_sec_websocket_extensions;" >> /etc/nginx/conf.d/plex.conf
        "proxy_set_header Sec-WebSocket-Key $http_sec_websocket_key;" >> /etc/nginx/conf.d/plex.conf**
        "proxy_set_header Sec-WebSocket-Version $http_sec_websocket_version;" >> /etc/nginx/conf.d/plex.conf

        "proxy_http_version 1.1;" >> /etc/nginx/conf.d/plex.conf
        "proxy_set_header Upgrade $http_upgrade;" >> /etc/nginx/conf.d/plex.conf
        "proxy_set_header Connection "Upgrade";" >> /etc/nginx/conf.d/plex.conf

        "proxy_redirect off;" >> /etc/nginx/conf.d/plex.conf
        "proxy_buffering off;" >> /etc/nginx/conf.d/plex.conf

        "location / {" >> /etc/nginx/conf.d/plex.conf
                "proxy_pass http://plex_backend;" >> /etc/nginx/conf.d/plex.conf
        "]" >> /etc/nginx/conf.d/plex.conf"
"}" >> /etc/nginx/conf.d/plex.conf
}

plex_conf

function nginx_conf(){
    touch /etc/nginx/nginx.conf
    "include /etc/nginx/sites-enabled/*;" >> /etc/nginx/nginx.conf
    "server_names_hash_bucket_size 64;" >> /etc/nginx/nginx.conf
"}" >> /etc/nginx/nginx.conf
}

nginx_conf

#Vérification d'erreur
nginx -t

systemctl restart nginx
systemctl status nginx

#Installation de netdata
bash <(curl -Ss https://my-netdata.io/kickstart.sh)

# Paramétrage de Netdata
systemctl start netdata
systemctl enable netdata
systemctl status netdata
systemctl is-enabled netdata

# Déterminer sur quel port écoute Netdata
ss -ltn

#autoriser le port dans le firewall
read -p "entrer le port utilisé " port

# Autorisé ce port dans le firewall
firewall-cmd --add-port=$port/tcp
```

#### [](#script-pour-linstallation-du-backup-cot%C3%A9-serveur-)Script pour l'installation du backup coté serveur :

```
Install_backup_serveur.sh

#!/bin/bash
# Install backup serveur
# "THEO & QUENTIN 11/12/2022"

echo "!! Veuillez installer ce script sur une machine dédiée au backup !!"

# créer le dossier de backup
mkdir /srv/backup/"VM Client"

# installation de nfs
dnf -y install nfs-utils

cat /etc/hostname
read -p "Veuillez indiquez le nom de votre machine :" machine

read -p "Veuillez indiquez votre IP:" ip

# Pamétrage de nfs
touch /etc/idmapd.con
sed -i -r 's/.*Domain.*/$machine/g' /etc/idmapd.con

touch /etc/exports
sed -i -r 's/.*(rw,no_root_squash).*//svr/backup/$machine $ip(rw,no_root_squash)/g' /etc/exports

# Activation du service
systemctl enable --now rpcbind nfs-server
firewall-cmd --add-service=nfs
```

#### [](#script-pour-linstallation-du-backup-cot%C3%A9-clients-)Script pour l'installation du backup coté clients :

```
Install_backup_client.sh

#!/bin/bash
# Install backup client
# "THEO & QUENTIN 11/12/2022"

# créer le dossier de backup
mkdir /srv/backup/"VM Client"

# installation de nfs
dnf -y install nfs-utils

cat /etc/hostname
read -p "Veuillez indiquez le nom de votre machine :" machine

# Pamétrage de nfs
touch /etc/idmapd.con
sed -i -r 's/.*Domain.*/$machine/g' /etc/idmapd.con

# Création d'une partition
df -h

read -p 'Reste-t-il de la place ?: ' validation
if [ $validation = "yes" ]
then 
    #Monter la partition automatiquement
    touch /etc/fstab
    read -p 'Nom de la machine backup ?: ' machine_backup
    "$machine_backup:/srv/backup/$machine /srv/backup/              nfs     defaults        0 0 " >> /etc/fstab
else
    exit 0
fi
```
