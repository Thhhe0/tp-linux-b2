# TP2 : Gestion de service

TP réalisé sous Debian

## I. Un premier serveur web

🌞 **Installer le serveur Apache**

```
theol@web:~$ sudo apt install apache 2 -y
```
🌞 **Démarrer le service Apache**

```
theol@web:~$ sudo systemctl start apache2
```

- faites en sorte qu'Apache démarre automatique au démarrage de la machine

```
theol@web:~$ sudo systemctl enable apache2
```

- ouvrez le port firewall nécessaire
```
theol@web:~$ sudo firewall-cmd --add-port=80/tcp --permanent
theol@web:~$ sudo firewall-cmd --reload
```

🌞 **TEST**

- vérifier que le service est démarré
```
theol@web:~$ sudo systemctl is-active apache2
active
```

-   vérifier qu'il est configuré pour démarrer automatiquement
```
theol@web:~$ sudo systemctl is-enabled apache2
enabled
```
-   vérifier avec une commande `curl localhost` que vous joignez votre serveur web localement
```
theol@web:~$ curl localhost

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
```

-   vérifier avec votre navigateur (sur votre PC) que vous accéder à votre serveur web
```
theo_l@Air-de-Theo ~ % curl 192.168.64.7

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
```

## 2. Avancer vers la maîtrise du service

🌞 **Le service Apache...**
- affichez le contenu du fichier `httpd.service` qui contient la définition du service Apache
```
theol@web:~$ sudo systemctl cat apache2
# /lib/systemd/system/apache2.service
[Unit]
Description=The Apache HTTP Server
[...]
```
🌞 **Déterminer sous quel utilisateur tourne le processus Apache**

-   mettez en évidence la ligne dans le fichier de conf principal d'Apache (`httpd.conf`) qui définit quel user est utilisé

```
theol@web:~$ sudo cat /etc/apache2/apache2.conf | grep User
User ${APACHE_RUN_USER}
```
-   utilisez la commande `ps -ef` pour visualiser les processus en cours d'exécution et confirmer que apache tourne bien sous l'utilisateur mentionné dans le fichier de conf
```
theol@web:~$ ps -ef | grep www
www-data    1303       1  0 Nov16 ?        00:00:02 /usr/bin/htcacheclean -d 120 -p /var/cache/apache2/mod_cache_disk -l 300M -n
www-data   12993    8048  0 00:00 ?        00:00:00 /usr/sbin/apache2 -k start
```
il tourne sous un autre nom

-   la page d'accueil d'Apache se trouve dans `/usr/share/testpage/`
    -   vérifiez avec un `ls -al` que tout son contenu est **accessible en lecture** à l'utilisateur mentionné dans le fichier de conf
   ```
   theol@web:~$ ls -al /usr/share/apache2/default-site/
total 20
drwxr-xr-x 2 root root  4096 Nov 15 14:38 .
drwxr-xr-x 6 root root  4096 Nov 15 14:38 ..
-rw-r--r-- 1 root root 10701 Jun  9 05:22 index.html
   ```

🌞 **Changer l'utilisateur utilisé par Apache**
- créez un nouvel utilisateur

-   pour les options de création, inspirez-vous de l'utilisateur Apache existant
    -   le fichier `/etc/passwd` contient les informations relatives aux utilisateurs existants sur la machine
    -   servez-vous en pour voir la config actuelle de l'utilisateur Apache par défaut

```
theol@web:~$ sudo cat /etc/passwd | grep www
www-data:x:33:33:www-data:/var/www:/usr/sbin/nologin

theol@web:~$ sudo useradd newuser -m -s /sbin/nologin -u 3000
theol@web:~$ sudo usermod -aG www-data newuser

theol@web:~$ sudo cat /etc/passwd | grep newuser
newuser:x:3000:3000::/home/newuser:/sbin/nologin
```

-   modifiez la configuration d'Apache pour qu'il utilise ce nouvel utilisateur
```
theol@web:~$ sudo cat /etc/apache2/apache2.conf | grep newuser
User  newuser
```
-   redémarrez Apache
```
theol@web:~$ ps -ef | grep newuser
```
-   utilisez une commande `ps` pour vérifier que le changement a pris effet
```
theol@web:~$ ps -ef | grep newuser
newuser    13221   13220  0 00:32 ?        00:00:00 /usr/sbin/apache2 -k start
newuser    13222   13220  0 00:32 ?        00:00:00 /usr/sbin/apache2 -k start
newuser    13223   13220  0 00:32 ?        00:00:00 /usr/sbin/apache2 -k start
newuser    13224   13220  0 00:32 ?        00:00:00 /usr/sbin/apache2 -k start
newuser    13225   13220  0 00:32 ?        00:00:00 /usr/sbin/apache2 -k start
```

🌞 **Faites en sorte que Apache tourne sur un autre port**

-   modifiez la configuration d'Apache pour lui demander d'écouter sur un autre port de votre choix
```
theol@web:~$ sudo cat /etc/apache2/apache2.conf | grep Listen
Listen 1000
```

-   ouvrez ce nouveau port dans le firewall, et fermez l'ancien
```
theol@web:~$ sudo firewall-cmd --add-port=1000/tcp --permanent
success
theol@web:~$ sudo firewall-cmd --remove-port=80/tcp --permanent
success
```
-   redémarrez Apache

```
theol@web:~$ sudo systemctl restart apache2
```
-   prouvez avec une commande `ss` que Apache tourne bien sur le nouveau port choisi
```
theol@web:~$ sudo ss -lanpt | grep 1000
sudo: unable to resolve host web.tp2.linux: Name or service not known
LISTEN 0      511                *:1000            *:*     users:(("apache2",pid=13930,fd=4),("apache2",pid=13929,fd=4),("apache2",pid=13928,fd=4),("apache2",pid=13927,fd=4),("apache2",pid=13926,fd=4),("apache2",pid=13925,fd=4))
```
-   vérifiez avec `curl` en local que vous pouvez joindre Apache sur le nouveau port
```theol@web:~$ curl localhost:1000

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
  [...]
```
-   vérifiez avec votre navigateur que vous pouvez joindre le serveur sur le nouveau port

```
theo_l@MacBook-Air-de-Theo ~ % curl 192.168.64.7:1000     

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
```

## II. Une stack web plus avancée

## 2. Setup
### A. Base de données

🌞 **Install de MariaDB sur `db.tp2.linux`**

```
theo@db:~$ sudo apt install mariadb-server
theo@db:~$ sudo systemctl enable mariadb
theo@db:~$ sudo systemctl start mariadb
theo@db:~$ sudo mysql_secure_installation
```
- vous repérerez le port utilisé par MariaDB avec une commande `ss` exécutée sur `db.tp2.linux`

```
theo@db:~$ sudo ss -lapnt | grep maria
sudo: unable to resolve host db.tp2.linux: Name or service not known
LISTEN 0      80         127.0.0.1:3306      0.0.0.0:*     users:(("mariadbd",pid=526,fd=19)) 
```
- il sera nécessaire de l'ouvrir dans le firewall
```
theo@db:~$ sudo firewall-cmd --add-port=3306/tcp --permanent
sudo: unable to resolve host db.tp2.linux: Name or service not known
success
```

🌞 **Préparation de la base pour NextCloud**

- une fois en place, il va falloir préparer une base de données pour NextCloud :

-   connectez-vous à la base de données à l'aide de la commande `sudo mysql -u root -p`
-   exécutez les commandes SQL suivantes :

```
theo@db:~$ sudo mysql -u root -p
Enter password: 
Welcome to the MariaDB monitor.  Commands end with ; or \g.
Your MariaDB connection id is 36
Server version: 10.5.15-MariaDB-0+deb11u1 Debian 11

Copyright (c) 2000, 2018, Oracle, MariaDB Corporation Ab and others.

Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.

MariaDB [(none)]> CREATE USER 'nextcloud'@'192.168.64.7' IDENTIFIED BY 'theodb';
Query OK, 0 rows affected (0.004 sec)

MariaDB [(none)]> CREATE DATABASE IF NOT EXISTS nextcloud CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
Query OK, 0 rows affected, 1 warning (0.001 sec)

MariaDB [(none)]> GRANT ALL PRIVILEGES ON nextcloud.* TO 'nextcloud'@'192.168.64.7';
Query OK, 0 rows affected (0.004 sec)

MariaDB [(none)]> FLUSH PRIVILEGES;
Query OK, 0 rows affected (0.001 sec)
```

🌞 **Exploration de la base de données**

-   afin de tester le bon fonctionnement de la base de données, vous allez essayer de vous connecter, comme NextCloud le fera :
    -   depuis la machine `web.tp2.linux` vers l'IP de `db.tp2.linux`
    -   utilisez la commande `mysql` pour vous connecter à une base de données depuis la ligne de commande
        -   par exemple `mysql -u <USER> -h <IP_DATABASE> -p`
```
theol@web:~$ mysql -u nextcloud -h 192.168.64.9 -p
Enter password: 
Welcome to the MariaDB monitor.  Commands end with ; or \g.
Your MariaDB connection id is 7
Server version: 10.5.15-MariaDB-0+deb11u1 Debian 11
```

-   **donc vous devez effectuer une commande `mysql` sur `web.tp2.linux`**
-   une fois connecté à la base, utilisez les commandes SQL fournies ci-dessous pour explorer la base
```
MariaDB [(none)]> SHOW DATABASES;
+--------------------+
| Database           |
+--------------------+
| information_schema |
| nextcloud          |
+--------------------+
2 rows in set (0.010 sec)

MariaDB [(none)]> USE nextcloud;
Database changed
MariaDB [nextcloud]> SHOW TABLES;
Empty set (0.004 sec)
```

🌞 **Trouver une commande SQL qui permet de lister tous les utilisateurs de la base de données**

-   vous ne pourrez pas utiliser l'utilisateur `nextcloud` de la base pour effectuer cette opération : il n'a pas les droits
-   il faudra donc vous reconnectez localement à la base en utilisant l'utilisateur `root`
```
MariaDB [(none)]> SELECT User,Host FROM mysql.user;
+-------------+--------------+
| User        | Host         |
+-------------+--------------+
| nextcloud   | 192.168.64.7 |
| mariadb.sys | localhost    |
| mysql       | localhost    |
| root        | localhost    |
+-------------+--------------+
4 rows in set (0.004 sec)
```

### B. Serveur Web et NextCloud

⚠️⚠️⚠️ **N'OUBLIEZ PAS de réinitialiser votre conf Apache avant de continuer. En particulier, remettez le port et le user par défaut.**

🌞 **Install de PHP**

```
theol@web:~$ sudo apt install php-common libapache2-mod-php php-cli
```
🌞 **Install de tous les modules PHP nécessaires pour NextCloud**
```
theol@web:~$ sudo apt install php php-gd php-curl php-zip php-dom php-xml php-simplexml php-mbstring php-intl php-bcmath php-gmp php-imagick php-mysql -y
```

🌞 **Récupérer NextCloud**

-   créez le dossier `/var/www/tp2_nextcloud/`
    -   ce sera notre _racine web_ (ou _webroot_)
    -   l'endroit où le site est stocké quoi, on y trouvera un `index.html` et un tas d'autres marde, tout ce qui constitue NextClo :D
```
theol@web:~$ sudo mkdir /var/www/tp2_nextcloud
```
récupérer le fichier suivant avec une commande `curl` ou `wget` : [https://download.nextcloud.com/server/prereleases/nextcloud-25.0.0rc3.zip](https://download.nextcloud.com/server/prereleases/nextcloud-25.0.0rc3.zip)
```
theol@web:~$ wget https://download.nextcloud.com/server/prereleases/nextcloud-25.0.0rc3.zip
```
extrayez tout son contenu dans le dossier `/var/www/tp2_nextcloud/` en utilisant la commande `unzip`

-   installez la commande `unzip` si nécessaire
- vous pouvez extraire puis déplacer ensuite, vous prenez pas la tête
```
theol@web:~$ sudo apt install unzip -y
theol@web:~$ sudo unzip nextcloud-25.0.0rc3.zip
theol@web:~$ sudo mv nextcloud/* /var/www/tp2_nextcloud/
```
-    contrôlez que le fichier `/var/www/tp2_nextcloud/index.html` existe pour vérifier que tout est en place

```
theol@web:~$ ls /var/www/tp2_nextcloud/ | grep index.html
index.html
```
-   assurez-vous que le dossier `/var/www/tp2_nextcloud/` et tout son contenu appartient à l'utilisateur qui exécute le service Apache
```
theol@web:~$ sudo ls -al /var/www/tp2_nextcloud
total 168
drwxr-xr-x 14 www-data www-data  4096 Nov 18 11:21 .
drwxr-xr-x  4 root     root      4096 Nov 18 11:14 ..
drwxr-xr-x 47 www-data www-data  4096 Oct  6 13:47 3rdparty
drwxr-xr-x 50 www-data www-data  4096 Oct  6 13:44 app
[...]
```

🌞 **Adapter la configuration d'Apache**

-   regardez la dernière ligne du fichier de conf d'Apache pour constater qu'il existe une ligne qui inclut d'autres fichiers de conf
-   créez en conséquence un fichier de configuration qui porte un nom clair et qui contient la configuration suivante :
```
theol@web:~$ sudo cat /etc/apache2/conf-enabled/nextcloud.conf 
<VirtualHost *:80>

DocumentRoot /var/www/tp2_nextcloud/

ServerName web.tp2.linux

<Directory /var/www/tp2_nextcloud/>

  Require all granted
  AllowOverride All
  Options FollowSymLinks MultiViews
  <IfModule mod_dav.c>
    Dav off
  </IfModule>
 </Directory>
</VirtualHost>
```
🌞 **Redémarrer le service Apache** pour qu'il prenne en compte le nouveau fichier de conf

```
theol@web:~$ sudo systemctl restart apache2
```
### C. Finaliser l'installation de NextCloud

➜ **Sur votre PC**

-   modifiez votre fichier `hosts` (oui, celui de votre PC, de votre hôte)
    -   pour pouvoir joindre l'IP de la VM en utilisant le nom `web.tp2.linux`

```
theo_l@MacBook-Air-de-Theo ~ % cat /etc/hosts | grep web
192.168.64.7	web.tp2.linux
```

- avec un navigateur, visitez NextCloud à l'URL `http://web.tp2.linux`

-   c'est possible grâce à la modification de votre fichier `hosts`
```
theo_l@MacBook-Air-de-Theo ~ % curl web.tp2.linux
theo_l@MacBook-Air-de-Theo ~ % curl web.tp2.linux

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
  [...]
```
🌞 **Exploration de la base de données**

-   connectez vous en ligne de commande à la base de données après l'installation terminée
-   déterminer combien de tables ont été crées par NextCloud lors de la finalisation de l'installation
```
theol@web:~$ sudo mysql -u nextcloud -h 192.168.64.9 -p
[...]
MariaDB [(none)]> SELECT COUNT(*) FROM information_schema.tables WHERE table_type = 'BASE TABLE';
+----------+
| COUNT(*) |
+----------+
|      124 |
+----------+
1 row in set (0.014 sec)
```



