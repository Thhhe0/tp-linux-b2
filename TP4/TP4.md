
# TP4 : Conteneurs

# I. Docker

🖥️ Machine **docker1.tp4.linux**

## 1. Install

🌞 **Installer Docker sur la machine**
```
theo@docker1:~$ sudo apt-get remove docker docker-engine docker.io containerd runc
theo@docker1:~$ sudo apt-get update
theo@docker1:~$ sudo apt-get install     ca-certificates     curl     gnupg     lsb-release
theo@docker1:~$ sudo mkdir -p /etc/apt/keyrings
theo@docker1:~$ curl -fsSL https://download.docker.com/linux/debian/gpg | sudo gpg --dearmor -o /etc/apt/keyrings/docker.gpg
theo@docker1:~$ echo   "deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/docker.gpg] https://download.docker.com/linux/debian \
  $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
theo@docker1:~$ sudo apt-get update
theo@docker1:~$ sudo apt-get install docker-ce docker-ce-cli containerd.io docker-compose-plugin
```
- démarrer le service `docker` avec une commande `systemctl`
```
theo@docker1:~$ sudo systemctl start docker
[sudo] password for theo: 
theo@docker1:~$ sudo systemctl status docker
```
- ajouter votre utilisateur au groupe `docker`

- -  cela permet d'utiliser Docker sans avoir besoin de l'identité de `root`
- - avec la commande : `sudo usermod -aG docker $(whoami)`
- - déconnectez-vous puis relancez une session pour que le changement prenne effet

```
theo@docker1:~$ sudo usermod -aG docker $(whoami)
```

## 2. Vérifier l'install

➜ **Vérifiez que Docker est actif est disponible en essayant quelques commandes usuelles :**

```
theo@docker1:~$ docker info
Client:
 Context:    default
 Debug Mode: false
 Plugins:
  app: Docker App (Docker Inc., v0.9.1-beta3)
  buildx: Docker Buildx (Docker Inc., v0.9.1-docker)
  compose: Docker Compose (Docker Inc., v2.12.2)
```
```
theo@docker1:~$ docker ps -a
CONTAINER ID   IMAGE         COMMAND    CREATED          STATUS                      PORTS     NAMES
f9e788df3667   hello-world   "/hello"   37 minutes ago   Exited (0) 37 minutes ago             vigilant_swirles
```

🌞 **Utiliser la commande `docker run`**
- **lancer un conteneur `nginx`**

-   l'app NGINX doit avoir un fichier de conf personnalisé
-   l'app NGINX doit servir un fichier `index.html` personnalisé
-   l'application doit être joignable grâce à un partage de ports
-   vous limiterez l'utilisation de la RAM et du CPU de ce conteneur
-   le conteneur devra avoir un nom
```
theo@docker1:~$ docker run --name web -d -v ~/myNginx.conf:/etc/nginx/conf.d/myNginx.conf -v ~/index.html:/var/www/tp4/index.html  -m"1g" --cpus="1.0" -p 8888:9999 nginx
```

# II. Images

La construction d'image avec Docker est basée sur l'utilisation de fichiers `Dockerfile`.

L'idée est la suivante :

-   vous créez un dossier de travail
-   vous vous déplacez dans ce dossier de travail
-   vous créez un fichier `Dockerfile`
    -   il contient les instructions pour construire une image
    -   `FROM` : indique l'image de base
    -   `RUN` : indique des opérations à effectuer dans l'image de base
-   vous exécutez une commande `docker build . -t <IMAGE_NAME>`
-   une image est produite, visible avec la commande `docker images`

## Exemple de Dockerfile et utilisation

Exemple d'un Dockerfile qui :

-   se base sur une image ubuntu
-   la met à jour
-   installe nginx

```
$ cat Dockerfile
FROM ubuntu

RUN apt update -y

RUN apt install -y nginx
```

Une fois ce fichier créé, on peut :
```
$ ls
Dockerfile
$ docker build . -t my_own_nginx 
$ docker images
$ docker run -p 8888:80 my_own_nginx nginx -g "daemon off;"
$ curl localhost:8888
$ curl <IP_VM>:8888
```

> La commande `nginx -g "daemon off;"` permet de lancer NGINX au premier-plan, et ainsi demande à notre conteneur d'exécuter le programme NGINX à son lancement.

Plutôt que de préciser à la main à chaque `docker run` quelle commande doit lancer le conteneur (notre `nginx -g "daemon off;"` en fin de ligne ici), on peut, au moment du `build` de l'image, choisir d'indiquer que chaque conteneur lancé à partir de cette image lancera une commande donneé.

Il faut, pour cela, modifier le Dockerfile :

```
$ cat Dockerfile
FROM ubuntu

RUN apt update -y

RUN apt install -y nginx

CMD [ "/usr/sbin/nginx", "-g", "daemon off;" ]
```

```
$ ls
Dockerfile

$ docker build . -t my_own_nginx

$ docker images

$ docker run -p 8888:80 my_own_nginx

$ curl localhost:8888
$ curl <IP_VM>:8888
```

[![Waiting for Docker](/it4lik/b2-linux-2022/-/raw/master/tp/4/pics/waiting_for_docker.jpg)](/it4lik/b2-linux-2022/-/raw/master/tp/4/pics/waiting_for_docker.jpg)

## 2. Construisez votre propre Dockerfile

🌞 **Construire votre propre image**




