package main

import (
	"fmt"
	"hangman/src"
	"hangman/src/assets/anim"
	"time"
)

func main() {
	src.HandleHelp(false)
	graphics := src.HandleGraphics()
	game := src.HandleLoad()
	res := game.Update(graphics)
	if res {
		if graphics {
			fmt.Println("Congrats!")
		} else {
			time.Sleep(time.Second * 2)
			anim.RunAnimPull(anim.BuildAnimPull("CONGRATS!"), game)
		}

	} else {
		fmt.Println("Game Over!")
		fmt.Printf("the word was %v\n", game.Word.Word)
	}
}
