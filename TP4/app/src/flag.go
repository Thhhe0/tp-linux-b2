package src

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"os"
	"strconv"
)

func HandleLoad() Game {
	index := -1
	for i, arg := range os.Args {
		if arg == "--startWith" {
			index = i
		}
	}

	if index > -1 && index != len(os.Args)-1 {
		fn := os.Args[index+1]
		game := Game{}
		rawSave, err := ioutil.ReadFile(fn)
		if err != nil {
			panic(err)
		}
		rawSave, err = DecryptCBC([]byte("a long key to crypt the hangman!"), rawSave)
		if err != nil {
			panic(err)
		}
		json.Unmarshal(rawSave, &game)
		os.Args = append(os.Args[:index], os.Args[index+2:]...)
		if game.SaveName == "" {
			fmt.Println("The datas of the save file aren't correct")
			os.Exit(1)
		}
		return game
	} else if index > -1 {
		fmt.Println("missing file name")
		os.Exit(1)
		return Game{}
	} else {
		return NewGame()
	}
}

func HandleWordDb() string {
	args := os.Args[1:]
	if len(args) == 1 {
		return args[0]
	} else {
		if len(args) > 1 {
			fmt.Println("too many arguments")
			os.Exit(1)
			return ""
		} else {
			fmt.Println("missing words db argument")
			os.Exit(1)
			return ""
		}
	}
}

func HandleGraphics() bool {
	for i, arg := range os.Args {
		if arg == "--lowGraphics" || arg == "-lg" {
			os.Args = append(os.Args[:i], os.Args[i+1:]...)
			return true
		}
	}
	return false
}

func HandleHelp(inGame bool) {
	if !inGame {
		for _, arg := range os.Args {
			if arg == "--help" || arg == "-h" {
				fmt.Println("go run . [options] [db]\n\toptions :\n\t\t--lowGraphics, -lg : do not use ascii letter to display the word\n\t\t--startWith [file name] : load the save stored in [file name]\nwrite \"help\" in game for more information\na link to the documentation of this program to see how he was created => https://martinj.notion.site/Hangman-Doc-cfa2fd737af54961b535defc02968d38")
				os.Exit(1)
			}
		}
	} else {
		fmt.Println("heart are your life (remaining attempt)\ncharacter : check if the character exist in the word and display it in the word if it exist, if not you lose 1 attempt\nword      : check if the word you guessed is the same as the word, if not you lose 2 attempt, if it is you win\nstop      : if you write stop you will save and quit, if you write a file name (without it extension) after stop it will save in this file (or create it if it doesn't exist), if not it will create or rewrite a generated savefile\na link to the documentation of this program to see how he was created => https://martinj.notion.site/Hangman-Doc-cfa2fd737af54961b535defc02968d38")
	}
}

func HandleStop(res string, game *Game) {
	if res[:4] == "stop" {
		if len(res) > 4 {
			game.SaveName = res[5:]
		}

		if game.SaveName == "" {
			dir, err := ioutil.ReadDir("./")
			if err != nil {
				panic(err)
			}
			game.SaveName = "save" + strconv.Itoa(len(dir))
		}

		saveQuit(*game)
	} else {
		return
	}
}

func saveQuit(game Game) {
	save, err := json.Marshal(game)
	if err != nil {
		panic(err)
	}

	save, err = EncryptCBC([]byte("a long key to crypt the hangman!"), save)
	if err != nil {
		panic(err)
	}

	err = ioutil.WriteFile(game.SaveName+".txt", save, 0644)
	if err != nil {
		panic(err)
	}

	fmt.Println("Game Saved in " + game.SaveName + ".txt.")
	os.Exit(1)
}
