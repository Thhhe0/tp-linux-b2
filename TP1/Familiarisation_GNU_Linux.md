# TP1 : (re)Familiaration avec un système GNU/Linux

## 0. Préparation de la machine

🌞 **Setup de deux machines Rocky Linux configurées de façon basique.**

- **un accès internet (via la carte NAT)**

 **Carte réseau dédiée :**
```
[theol@node1 ~]$ ip a | grep "enp0s6" 
2: enp0s6: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
link/ether 96:a9:ec:39:60:8e brd ff:ff:ff:ff:ff:ff 
inet 192.168.64.4/24 brd 192.168.64.255 scope global dynamic noprefixroute enp0s6
```
 **Route par défaut :**
```
[theol@node1 ~]$ ip route s 
192.168.64.4/24 dev enp0s6 proto kernel scope link src 192.168.64.4 metric 100
```

- **un accès à un réseau local** (les deux machines peuvent se `ping`) (via la carte Host-Only)

```
[theol@node1 ~]$ ip a | grep "enp0s6" 
2: enp0s6: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
link/ether 96:a9:ec:39:60:8e brd ff:ff:ff:ff:ff:ff 
inet 192.168.64.4/24 brd 192.168.64.255 scope global dynamic noprefixroute enp0s6
```

-   **vous n'utilisez QUE `ssh` pour administrer les machines**
    
-   **les machines doivent avoir un nom**
```
[theol@node1 ~]$ sudo nano /etc/hostname 
[theol@node1 ~]$ cat /etc/hostname 
node1.tp1.b2
```
- **utiliser `1.1.1.1` comme serveur DNS**
```
[theol@node1 ~]$ sudo nano /etc/resolv.conf 
[theol@node1 ~]$ cat /etc/resolv.conf | grep nameserver 
nameserver 1.1.1.1
```
- vérifier avec le bon fonctionnement avec la commande `dig`

 -   avec `dig`, demander une résolution du nom `ynov.com`
 -   mettre en évidence la ligne qui contient la réponse : l'IP qui correspond au nom demandé

 ```
[theol@node1 ~]$ dig ynov.com | grep "ynov.com" 
[...] 
ynov.com. 285 IN A 172.67.74.226 
ynov.com. 285 IN A 104.26.10.233 
ynov.com. 285 IN A 104.26.11.233
 ```
 

-   mettre en évidence la ligne qui contient l'adresse IP du serveur qui vous a répondu
```
[theol@node1 ~]$ dig ynov.com | grep SERVER: 
;; SERVER: 1.1.1.1#53(1.1.1.1)
```



- **les machines doivent pouvoir se joindre par leurs noms respectifs**
- - fichier `/etc/hosts`
```
[theol@node1 ~]$ sudo nano /etc/hosts 
192.168.64.5 usert
```
-- assurez-vous du bon fonctionnement avec des `ping <NOM>`
```
[theol@node1 ~]$ ping usert 
PING usert (192.168.64.5) 56(84) bytes of data. 
64 bytes from usert (192.168.64.5): icmp_seq=1 ttl=64 time=3.55 ms
```
- **le pare-feu est configuré pour bloquer toutes les connexions exceptées celles qui sont nécessaires**
 - - commande `firewall-cmd`
```
[theol@node1 ~]$ systemctl status firewalld | grep Active: 
Active: active (running) since Mon 2022-11-14 17:51:11 CET; 1h 18min ago
```
## I. Utilisateurs
### 1. Création et configuration

🌞 **Ajouter un utilisateur à la machine**, qui sera dédié à son administration

- précisez des options sur la commande d'ajout pour que :

- -   le répertoire home de l'utilisateur soit précisé explicitement, et se trouve dans `/home`
- -   le shell de l'utilisateur soit `/bin/bash`
```
[theol@node1 ~]$ sudo useradd usertp -m -s /bin/bash -u 1500
```
- prouvez que vous avez correctement créé cet utilisateur

- - et aussi qu'il a le bon shell et le bon homedir

```
[theol@node1 ~]$ cat /etc/passwd | grep usertp
 usertp:x:1500:1500::/home/usertp:/bin/bash
```

🌞 **Créer un nouveau groupe `admins`** qui contiendra les utilisateurs de la machine ayant accès aux droits de `root` _via_ la commande `sudo`.
```
[theol@node1 ~]$ sudo groupadd admins
[theol@node1 ~]$ sudo usermod -a -G admins usertp
```
Pour permettre à ce groupe d'accéder aux droits `root` :

-   il faut modifier le fichier `/etc/sudoers`
-   on ne le modifie jamais directement à la main car en cas d'erreur de syntaxe, on pourrait bloquer notre accès aux droits administrateur
-   la commande `visudo` permet d'éditer le fichier, avec un check de syntaxe avant fermeture
-   ajouter une ligne basique qui permet au groupe d'avoir tous les droits (inspirez vous de la ligne avec le groupe `wheel`)
```
[theol@node1 ~]$ sudo visudo /etc/sudoers
[theol@node1 ~]$ sudo cat /etc/sudoers | grep "%admins"
 %admins  ALL=(ALL)     ALL
```
🌞 **Ajouter votre utilisateur à ce groupe `admins`**
```
[theol@node1 ~]$ sudo ls /etc
 authselect                  hostname                   nsswitch.conf                   ssl
 [...]
```
## 2. SSH
Afin de se connecter à la machine de façon plus sécurisée, on va configurer un échange de clés SSH lorsque l'on se connecte à la machine.

🌞 **Pour cela...**

-   il faut générer une clé sur le poste client de l'administrateur qui se connectera à distance (vous :) )
    -   génération de clé depuis VOTRE poste donc
```
[theol@node1 ~]$ ssh-keygen -t rsa
```
déposer la clé dans le fichier `/home/<USER>/.ssh/authorized_keys` de la machine que l'on souhaite administrer

-   vous utiliserez l'utilisateur que vous avez créé dans la partie précédente du TP
-   on peut le faire à la main
-   ou avec la commande `ssh-copy-id`

```
theo_l@Air-de-Theo ~ % ssh-copy-id theol@192.168.64.4
  [...]
  Number of key(s) added: 1
  [...]
```

🌞 **Assurez vous que la connexion SSH est fonctionnelle**, sans avoir besoin de mot de passe.
```
theo_l@Air-de-Theo ~ % ssh theol@192.168.64.4
Activate the web console with: systemctl enable --now cockpit.socket

Last login: Mon Nov 14 20:12:05 2022 from 192.168.64.1
```


## II. Partitionnement
### 1. Préparation de la VM

⚠️ **Uniquement sur `node1.tp1.b2`.**


### 2. Partitionnement
